<?php


return [
    'components'=>[
        'widget'=>[
            'class'=>'yii2portal\sphinx\components\widget\Widget'
        ],
        'similar'=>[
            'class'=>'yii2portal\sphinx\components\similar\Similar'
        ],
        'search' => [
            'class' => 'yii\sphinx\Connection',
            'dsn' => 'mysql:host=127.0.0.1;port=9306;',
            'username' => '',
            'password' => '',
        ],
        'sphinx' => [
            'class' => 'yii\sphinx\Connection',
            'dsn' => 'mysql:host=127.0.0.1;port=9306;',
            'enableSchemaCache'=>true,
            'username' => '',
            'password' => '',
        ],
    ],
];