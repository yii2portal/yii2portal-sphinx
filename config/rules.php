<?php


return [
    [
        'pattern' => "autocomplete",
        'route' => "index/autocomplete"
    ],
    [
        'pattern' => "",
        'route' => "index/index",
        'defaults' => [
            'page' => 1
        ]
    ],
    [
        'pattern' => "page_<page:\d+>",
        'route' => "index/index",
        'defaults' => [
            'page' => 1
        ]
    ],
];