<?php

namespace yii2portal\sphinx\controllers;

use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\sphinx\ActiveDataProvider;
use yii2portal\core\controllers\Controller;
use yii2portal\news\models\Authors;
use yii2portal\sphinx\models\News;
use yii2portal\sphinx\models\SearchForm;

/**
 * Site controller
 */
class IndexController extends Controller
{

    public function behaviors()
    {
        return [
            'contentNegotiator' => [
                'class' => \yii\filters\ContentNegotiator::className(),
                'only' => ['autocomplete'],
                'formatParam' => '_format',
                'formats' => [
                    'application/json' => \yii\web\Response::FORMAT_JSON,
                    'application/xml' => \yii\web\Response::FORMAT_XML,
                ],
            ],
        ];
    }

    public function actionIndex($structure_id)
    {
        $sphinx = Yii::$app->getModule('sphinx')->sphinx;


        $query = News::find()->with('object', 'object.imageLenta');
        $query->addSelect(new Expression("WEIGHT() as weight, *"));
        $model = new SearchForm();
        $query->orderBy([
            $model->sortby=>intval($model->direction)
        ]);

        if ($model->load(Yii::$app->request->get())) {
            $query->andWhere("is_del=:isDel AND status=:status", [
                'isDel'=>0,
                'status'=>1
            ])->andWhere("datepublic<:time", [
                'time'=>time()
            ]);
            if($model->category){
                $pids = array_map('intval', $model->category);
                $query->andWhere("pid IN (:pids)", [
                    'pids'=> $pids
                ]);
            }

            $query->orderBy([
                $model->sortby=>intval($model->direction)
            ]);



            $matches = [];

            if($model->text){
                $matches[':text'] = $sphinx->escapeMatchValue($model->text);
            }

            if ($model->author) {
                $matches[':author'] = "@(authors_s) " . $sphinx->escapeMatchValue($model->author);
            }

            if (!empty($matches)) {
                $query->match(new Expression(implode(" ", array_keys($matches)), $matches));
            }

        }

        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 30,
            ]
        ]);


        return $this->render('index', [
            'page' => Yii::$app->modules['structure']->getPage($structure_id),
            'provider' => $provider,
            'model' => $model
        ]);
    }


    public function actionAutocomplete()
    {
        $term = Yii::$app->request->get('term');
        $query = Authors::find()->where(['like', 'name', $term])->limit(5);
        $namesList = ArrayHelper::getColumn($query->all(), 'name');
        return $namesList;
    }

}
