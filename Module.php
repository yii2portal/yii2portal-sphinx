<?php

namespace yii2portal\sphinx;

use Yii;

class Module extends \yii2portal\core\Module
{

    public $controllerNamespace = 'yii2portal\sphinx\controllers';

    public function init()
    {
        parent::init();
        Yii::$app->set('sphinx', $this->sphinx);
    }
}