<?php


namespace yii2portal\sphinx\components\similar;

use Yii;
use yii\db\Exception;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii2portal\core\components\Widget;
use yii2portal\sphinx\models\News;


class Similar extends Widget
{

    public function insert($content, $view, $notIds = null, $limit = 10, $any = false)
    {
        try {
            $sphinx = Yii::$app->getModule('sphinx')->sphinx;

            $content = $sphinx->escapeMatchValue(strip_tags($content));
            $query = News::find()->with('object', 'object.imageLenta');
            $query->andWhere("is_del=:isDel AND status=:status", [
                'isDel' => 0,
                'status' => 1
            ]);
            $query->match(new Expression(':text', [
                ':text' => !$any ? $content : '"' . $content . '"/1'
            ]));

            if ($notIds) {
                $query->andWhere("id NOT IN(:ids)", [
                    'ids' => $notIds
                ]);
            }

            $query->limit($limit);

            $data = $query->all();

            ArrayHelper::multisort($data, 'datepublic', SORT_DESC);


            return $this->render($view, [
                'data' => $data
            ]);
        }
        catch(Exception $e){

        }

    }
}