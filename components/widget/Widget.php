<?php


namespace yii2portal\sphinx\components\widget;

use Yii;
use yii2portal\core\components\Widget as YiiWidget;
use yii2portal\sphinx\models\SearchForm;


class Widget extends YiiWidget
{
    public function insert($view)
    {
        $model = new SearchForm();
        $model->load(Yii::$app->request->get());

        return $this->render($view, [
            'model'=>$model
        ]);
    }
}