<?php

namespace yii2portal\sphinx\models;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class SearchForm extends Model
{
    public $text;
    public $author;
    public $category;

    public $sortby = 'datepublic';
    public $direction = SORT_DESC;

    public function rules()
    {
        $newsCategories = Yii::$app->getModule('structure')->getPagesByModule('news');

        return [
            [['text', 'author'], "string"],
            ['category', 'each', 'rule' => [ 'in', 'range' => ArrayHelper::getColumn($newsCategories, 'id')]],
            ['direction', 'in', 'range'=>[SORT_ASC, SORT_DESC]],
            ['sortby', 'in', 'range'=>array_keys($this->orders())],
        ];
    }

    public function orders(){
        return [
            'datepublic'=>'Дата публикации',
            'weight'=>'Релевантность',
        ];
    }

    public function attributeLabels()
    {
        return [
            'text' => 'Искать по фразе',
            'author' => 'Автор',
            'category' => 'Раздел',
        ];
    }
}