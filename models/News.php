<?php

namespace yii2portal\sphinx\models;

use Yii;

/**
 * This is the model class for index "news".
 *
 * @property string $id
 * @property string $title
 * @property string $description
 * @property string $content
 * @property string $author
 * @property string $authors_s
 * @property string $pid
 * @property string $datepublic
 * @property string $is_del
 * @property string $status
 * @property array $author_id
 */
class News extends \yii\sphinx\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function indexName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'unique'],
            [['id', 'pid', 'is_del', 'status'], 'integer'],
            [['title', 'description', 'content', 'author', 'authors_s'], 'string'],
            [['datepublic', 'author_id'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'content' => 'Content',
            'author' => 'Author',
            'authors_s' => 'Authors S',
            'pid' => 'Pid',
            'datepublic' => 'Datepublic',
            'is_del' => 'Is Del',
            'status' => 'Status',
            'author_id' => 'Author ID',
        ];
    }

    public function getObject(){
        return $this->hasOne(\yii2portal\news\models\News::className(),['id'=>'id']);
    }

}
